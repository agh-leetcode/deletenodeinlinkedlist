/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
class Solution {
    public void deleteNode(ListNode node) {
        ListNode n1 = null;
	    n1 = node.next;
	    if (n1 != null) {
		    node.val = n1.val;
		    node.next = n1.next;
	    }
    }
}